<!-- TITLE: Inkwater & The Rest -->
<!-- SUBTITLE: 50.116.119.225 -->


# Login

# Sites
## Author Sites
allthatremainsnovel.com
andreamckenzieraine.com
anirishexperiencebook.com
edgeoffreedom.net
~~edwinvrell.com~~
firstbooks.com
howardgfranklin.com
~~inkwater-old.com~~
inkwater.com
inkwaterbooks.com
inkwaterpress.com
inkwaterpublicity.com
isabelcamachodiamond.com
itsonlyhairbook.com
jamessajo.com
jeansorrell.com
jeremysolomon.com
jimmoorebooks.com
journeyor7.com
meetingmrwrong.com
newcomers-web.com
newcomershandbooks.com
newcomersweb.com
officewiki.com
shopfirstbooks.com
~~solomonexamprep.com~~
stevemarantz.com


# ink.Inkwater.com Server
IP Address:  50.116.119.225

## **ClamScan**
<span style="float:right">
<img src="/uploads/clamav-trademark.png" />
</span> 

*Ink.inkwater.com   uses ClamScan to scan server for viruses.*

**ClamScan Website:**  https://www.clamav.net/

**Update Virus Defs: ** "freshclam"  (at command prompt)

**Command List:** https://www.mankier.com/1/clamscan



### **Commands:**

Scan all "/home" files and print the results to scan_log.txt, and move all infected files to /home/_infected
**clamscan -r /home  --max-filesize=100M --exclude=/.cpanel  --exclude=/_infected --exclude=/virtfs --exclude=/cache --move=/home/_infected  | grep FOUND >> /home/scan_report.txt**


Scan all "/home/\*/public_html" files only and log the results  (erases the 'log' and 'report' txt files)
**> /home/scan_log.txt && > /home/scan_report.txt  && clamscan -r /home/\*/public_html   --max-filesize=100M --log=/home/scan_log.txt --exclude=/cache --exclude=/_infected --move=/home/_infected  | grep FOUND >> /home/scan_report.txt**


