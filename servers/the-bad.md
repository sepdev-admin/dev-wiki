<!-- TITLE: The Bad - 192.168.1.109 -->

# System
Ubuntu 16.04.3
PHP 7.0
# Login
SSH User: webdev
SSH Password: IHateWordPress

MySQL User: root
MySQL Password: IHateWordPress

Root Password: IHateWordPress

# Sites
**Dev Wiki**
URL: http://dev-wiki  or 192.168.1.109:3000
Document Root: /var/www/dev-wiki

**Office Wiki**
URL: http://office-wiki
Document Root: /var/www/officewiki

**Inkwater (Dev)**
URL: Not currently running
Document Root: /var/www/inkwater
Databases: wp_inkwater, wp_inkwater_books
