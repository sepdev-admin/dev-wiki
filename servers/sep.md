<!-- TITLE: Solomon Exam Prep (SEP) -->
<!-- SUBTITLE: 52.35.173.227 -->

# System
Amazon Linux AMI release 2018.03
PHP 7.0.3
# Logins
SSH User: ec2-user
SSH Key: septest.pem
SSH with key syntax: ssh -i /path/to/septest.pem ec2-user@52.35.173.227
Copy the pem key to your current directory: scp webdev@192.168.1.109:/var/www/keys/septest.pem ./

MySQL User: sepv2db
MySQL Password: See development-sep/application/config/database.php
MySQL Endpoint: sepdb-next-gen2.cvuyxtzurfpa.us-west-2.rds.amazonaws.com
# Sites
**SEP** (Production)
URL: https://www.solomonexamprep.com
Document Root: /var/www/maxio-sep
Databases: sepv2_data, sepv2_wp

**SEP** (Dev)
URL: https://next-gen-staging-lb-758108099.us-west-2.elb.amazonaws.com/
Document Root: /var/www/local-prod-sep
Databases: sepv2_data, sepv2_wp **(CAREFUL! SHARED WITH PRODUCTION)**