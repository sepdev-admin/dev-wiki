<!-- TITLE: The Good - 192.169.1.112 -->

# System
Ubuntu 16.04.3
PHP 7.2

# Login
SSH User: dev
SSH Password: ILuvPHP

MySQL User: root
MySQL Password: ILuvPHP

Root Password: IAmDumb

# Sites
**SEP (dev)**
URL: http://development-sep-dev
Document Root: /var/www/development-sep
Databases: sepv2_data, sepv2_wp

**Author Portal (dev)**
Client URL: http://portal/
Document Root: /var/www/portal-client/dist

API URL: http://ap-api/
Document Root: /var/www/portal-api/public
Database: authordatabase-current