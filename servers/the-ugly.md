<!-- TITLE: The Ugly - 192.168.1.110 -->

# System
Ubuntu 12.04.5
PHP 5.3
# Login
SSH User: root
SSH Password: 1stb00ks!

MySQL User: root
MySQL Password: 1stb00ks!
# Sites
**Author Database**
URL: http://adb
Document Root: /var/www/production/adb
Database: authordatabase

**Iweb**
URL: http://iweb
Document Root: /var/www/production/iweb
Description: Chore manager and publishing calculator
Database: iweb_db

**Marketing Database**
URL: http://marketingdb
Document Root: /var/www/production/marketingdb
Database: marketingdatabase

**Jira** (No longer in use)
URL: http://192.168.1.110:8080

