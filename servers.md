<!-- TITLE: Servers -->

# Local Servers
## The Good
[192.168.112](http://192.168.1.109:3000/servers/the-good)
## The Bad
[192.168.109](http://192.168.1.109:3000/servers/the-bad)
## The Ugly
[192.168.110](http://192.168.1.109:3000/servers/the-ugly)

# Remote Servers
## SEP
[52.35.173.227](http://192.168.1.109:3000/servers/sep)
## Inkwater & The Rest
[50.116.119.225](http://192.168.1.109:3000/servers/inkwater-and-the-rest)